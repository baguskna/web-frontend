import React, { Component } from 'react';
import LandingComponent from '../component/LandingComponent';
import HeaderComponent from '../component/HeaderComponent';
import SideDrawerComponent from '../component/SideDrawerComponent';
import BackdropComponent from '../component/BackdropComponent';

class MainPage extends Component {
  state ={
    sideDrawerOpen: false
  };
  drawerToggleClickHandler = () => {
    this.setState((prevState) => {
      return {sideDrawerOpen: !prevState.sideDrawerOpen}
    });
  };

  backdropClickHandler = () => {
    this.setState({
      sideDrawerOpen: false
    });
  };

  render() {
    let backdrop;

    if (this.state.sideDrawerOpen) {
      backdrop = <BackdropComponent click={this.backdropClickHandler} />
    }
    return (
      <div style={{ height: '100%'}}>
        <HeaderComponent drawerClickHandler={this.drawerToggleClickHandler}/>
        <SideDrawerComponent show={this.state.sideDrawerOpen} />
        {backdrop}
        <LandingComponent />
      </div>
    )
  }
}

export default MainPage;