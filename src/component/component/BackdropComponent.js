import React from 'react';
import '../../assets/scss/BackdropComponent.scss';

const backdropComponent = props => (
  <div className="backdrop" onClick={props.click}/>
);

export default backdropComponent;