import React, { Component } from 'react';
import '../../assets/scss/LandingComponent.scss'

class LandingComponent extends Component {
  render() {
    return(
      <div className="jumbotron--section">
        <div className="jumbotron--container">
          <div className="jumbotron--title">
            <h1>Hi world!!</h1>
            <h1>I am Bagus</h1>
            <p>
              I just attended my four years university this year.
              My Major is Telecommunication Engineering.
              I have knowledge in Front-End and Android Development
            </p>
          </div>
          <div className="jumbotron--img">
            <img src={require("../../assets/images/album_bagus.JPG")} alt="Superior REDU" />
          </div>
        </div>
      </div>
    )
  }
}

export default LandingComponent;